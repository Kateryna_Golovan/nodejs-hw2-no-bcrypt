const express = require('express');
const router = new express.Router();

const {
    registration,
    signIn
} = require('../services/authService');

const {
    asyncWrapper
} = require('../utils/apiUtils');

router.post('/register', asyncWrapper(async (req, res) => {
    const {
        username,
        password
    } = req.body;
    if (!username || !password) {
            throw new InvalidRequestError('Invalid username or password');
    }

    await registration({username, password});

    res.json({message: 'Account created successfully!'});
}));

router.post('/login', asyncWrapper(async (req, res) => {
    const {
        username,
        password
    } = req.body;

    const token = await signIn({username, password});

    res.json({token, message: 'Logged in successfully!'});
}));

module.exports = {
    authRouter: router
}
