const {Note} = require('../models/noteModel');

const getNotesByUserId = async (userId) => {
    const notes = await Note.find({userId});
    return notes;
}

const getNoteByIdForUser = async (noteId, userId) => {
    const note = await Note.findOne({_id: noteId, userId});
    return note;
}

const addNoteToUser = async (userId, notePayload) => {
    const note = new Note({...notePayload, userId});
    await note.save();
}

const updateNoteByIdForUser = async (noteId, userId, data) => {
    await Note.findOneAndUpdate({_id: noteId, userId}, { $set: data});
}

const changeCompletedFNoteByIdForUser = async (noteId, userId, data) => {
    const completed = note.completed ? false : true;
    await Note.findOneAndUpdate({_id: id, userId}, {completed});
}

const deleteNoteByIdForUser = async (noteId, userId) => {
    await Note.findOneAndRemove({_id: noteId, userId});
}

module.exports = {
    getNotesByUserId,
    getNoteByIdForUser,
    addNoteToUser,
    updateNoteByIdForUser,
    changeCompletedFNoteByIdForUser,
    deleteNoteByIdForUser
};